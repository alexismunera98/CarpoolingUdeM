package com.udem.sw.carpooling;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.AccessToken;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.login.LoginManager;
import com.udem.sw.carpooling.Entities.User;
import com.udem.sw.carpooling.Request.UserRequest;
import com.udem.sw.carpooling.Callbacks.GetUserCallback;

public class MainActivity extends AppCompatActivity implements GetUserCallback.IGetUserResponse{

    private SimpleDraweeView profileView;
    private TextView mName, mId, mEmail, mPermissions;
    private Button btnLogout, btnMaps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(this);
        setContentView(R.layout.activity_main);

        profileView = findViewById(R.id.profilePhoto);
        mName = findViewById(R.id.name);
        mId = findViewById(R.id.id);
        mEmail = findViewById(R.id.email);
        mPermissions = findViewById(R.id.permissions);
        btnLogout = findViewById(R.id.btnLogout);
        btnMaps = findViewById(R.id.btnMaps);


        if (AccessToken.getCurrentAccessToken() == null) {
            Intent loginIntent = new Intent(MainActivity.this, FacebookLoginActivity.class);
            startActivity(loginIntent);
        }

        btnMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mapsIntent = new Intent(MainActivity.this, MapsActivity.class);
                startActivity(mapsIntent);
            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logOut();
                Intent loginActivity = new Intent(MainActivity.this, FacebookLoginActivity.class);
                startActivity(loginActivity);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        UserRequest.makeUserRequest(new GetUserCallback(MainActivity.this).getCallback());
    }

    @Override
    public void onCompleted(User user) {
        profileView.setImageURI(user.getPicture());
        mName.setText(user.getName());
        mId.setText(user.getId());
        if (user.getEmail() == null) {
            mEmail.setText("No hay permisos de Email");
            mEmail.setTextColor(Color.RED);
        } else {
            mEmail.setText(user.getEmail());
            mEmail.setTextColor(Color.BLACK);
        }
        mPermissions.setText(user.getPermissions());
    }
}
