# Carpooling UdeM

- Login usando Facebook SDK
- Captura de la localización actual del dispositivo usando GMaps SDK

## Instalación:
1. Descargar APK desde Edmodo
2. Permitir aplicaciones de terceros en los ajustes del dispositivo
3. Instalar aplicación
4. Loguear con Facebook
5. Activar ubicación del dispositivo
6. Otorgar permisos de ubicación cuando sea solicitado

### Recomendaciones:
* Android 5.0 o superior
* Probado en Android 7.0


#### En caso de querer compilar la aplicación:
1. Instalar Android Studio
2. Instalar SDK/API 27
3. Sincronizar el proyecto para descargar las dependencias